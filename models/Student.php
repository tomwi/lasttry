<?php

namespace app\models;
use yii\db\ActiveRecord;

class Student extends ActiveRecord
{

   
    public $name;
    public $id;
 
        public static function tableName(){
        return 'student';
    }
    
        public function getFirstName($id){
        return $this->getAttribute('firstName');
    }
    
        public function getLastName($id){
        return $this->getAttribute('lastName');
    }
          

}
